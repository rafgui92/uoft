**Respository**


---

## [Assignment 1](https://uoft.rafgui.com/public/docs/assignment1.pdf)

1. *Setting up your development environment.*

The first emulator chosen for was an android emulator in google called ARC Welde, the second one was the iOS emulator. 

2. *HTML5-validated form.*

The html form can be found in the following link, and also we will find a second link which will take us to the git repository where the source code of the first assignment is and it will be updated according to the other assignments.

Link Webpage: 

[https://uoft.rafgui.com/assignment1/index.html](https://uoft.rafgui.com/assignment1/index.html)

Link Repository:

[https://bitbucket.org/rafgui92/uoft](https://bitbucket.org/rafgui92/uoft)


3. *Using PhoneGap Build.*

The following link is has the access to download the native application in android of the point 2 of the assignment :
[https://build.phonegap.com/apps/3335939/install/EMF-ZwJ18XcXWPyE1MPR](https://build.phonegap.com/apps/3335939/install/EMF-ZwJ18XcXWPyE1MPR)

---
