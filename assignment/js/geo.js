if (Modernizr.geolocation) {
    alert( "We want to use your position to offer you a better experience ?" );
    
    mapboxgl.accessToken = 'pk.eyJ1IjoicmFmZ3VpMDEyIiwiYSI6ImNqcDhmYXp4eTB3ZGszcW83Z2N4bHhtdmoifQ.dOaqoKVusfXHCXds-Bcpgg';
    var map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v9',
        center: [-96, 37.8], // starting position
        zoom: 3 // starting zoom
    });

// Add geolocate control to the map.
    map.addControl(new mapboxgl.GeolocateControl({
        positionOptions: {
            enableHighAccuracy: true
        },
        trackUserLocation: true
    }));
    
} else {
    console.log("NO");
}
